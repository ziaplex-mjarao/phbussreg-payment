/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment.gcash;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Marlon Janssen Aro
 */
public final class Request {
    
    private static final String EMPTYSTRING = "";
    
    private JSONObject jsonObj = new JSONObject();
    
    private String id = EMPTYSTRING;
    private String pin = EMPTYSTRING;
    private String trn = EMPTYSTRING;
    private double amount;
    private String target = EMPTYSTRING;

    public String getId() { return id; }
    public void setId(String id) { if (id != null) this.id = id; }
    public String getPin() { return pin; }
    public void setPin(String pin) { if (pin != null) this.pin = pin; }
    public String getTrn() { return trn; }
    public void setTrn(String trn) { if (trn != null) this.trn = trn; }
    public double getAmount() { return this.amount; }
    public void setAmount(double amount) { 
        if (amount >= 0) this.amount = amount; }
    public String getTarget() { return target; }
    public void setTarget(String target) { 
        if (target != null) this.target = target; }
    public String getMobileNumber() { return target; }
    public void setMobileNumber(String mobileNo) { 
        if (mobileNo != null) this.target = mobileNo; }
    
    public String toJSONString() {
        if (!this.trn.isEmpty()) jsonObj.put("trn", this.trn);
        if (!id.isEmpty() && !pin.isEmpty() && 
                amount >= 0 && !target.isEmpty()) {
            jsonObj.put("id", this.id);
            jsonObj.put("pin", this.pin);
            jsonObj.put("amount", String.valueOf(amount));
            jsonObj.put("target", this.target);
        }
        return jsonObj.toString();
    }
    
    public void fromJSONString(String request) {
        if (request != null && !request.isEmpty()) {
            try {
                jsonObj = new JSONObject(request);
                if (jsonObj.has("id")) this.id = jsonObj.getString("id");
                if (jsonObj.has("pin")) this.pin = jsonObj.getString("pin");
                if (jsonObj.has("trn")) this.trn = jsonObj.getString("trn");
                if (jsonObj.has("amount")) 
                    this.amount = jsonObj.getDouble("amount");
                if (jsonObj.has("target")) 
                    this.target = jsonObj.getString("target");
            } catch (JSONException ex) { throw new RuntimeException(ex); }
        }
    }
}
