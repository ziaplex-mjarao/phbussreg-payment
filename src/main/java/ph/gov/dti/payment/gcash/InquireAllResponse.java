/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment.gcash;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Marlon Janssen Arao
 */
public class InquireAllResponse extends Response implements Serializable {
    
    private static final long serialVersionUID = 9081478620783329815L;
    
    private List<Transaction> transactions = new ArrayList<>();

    public List<Transaction> getTransactions() { return transactions; }
    public void setTransactions(List<Transaction> transactions) { 
        if (transactions != null) this.transactions = transactions; }

    @Override
    public void fromJSONString(String response) {
        if (response != null && !response.isEmpty()) {
            try {
                JSONObject resp = new JSONObject(response);
                this.setCode(resp.getInt("code"));
                this.setMessage(resp.getString("message"));
                JSONArray txns = resp.getJSONArray("transactions");
                JSONObject txn = null;
                this.transactions = new ArrayList<>();
                Transaction tx = new Transaction();
                for (Object t : txns) {
                    txn = (JSONObject) t;
                    tx = new Transaction();
                    tx.setMessage(txn.getString("message"));
                    tx.setCode(txn.getInt("code"));
                    tx.setTransId(txn.getLong("transid"));
                    tx.setTrn(txn.getString("trn"));
                    tx.setTarget(txn.getString("target"));
                    tx.setAmount(txn.getDouble("amount"));
                    tx.setTimestamp(txn.getString("timestamp"));
                    this.transactions.add(tx);
                }
            } catch (JSONException ex) { throw new RuntimeException(ex); }
        }
    }
}
