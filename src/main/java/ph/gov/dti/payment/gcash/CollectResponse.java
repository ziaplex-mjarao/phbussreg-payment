/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment.gcash;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class CollectResponse extends Response implements Serializable {
    
    private static final long serialVersionUID = 450418388904485826L;
    
    private long transid; 
    private Transaction latestTransaction = new Transaction();

    public long getTransId() { return transid; }
    public void setTransId(long transId) { this.transid = transId; }
    public Transaction getLatestTransaction() { return latestTransaction; }
    public void setLatestTransaction(Transaction latest) { 
        if (latest != null) this.latestTransaction = latest; }
    
    @Override
    public void fromJSONString(String response) {
        if (response != null && !response.isEmpty()) {
            try {
                JSONObject resp = new JSONObject(response);
                this.setCode(resp.getInt("code"));
                this.setMessage(resp.getString("message"));
                this.transid = resp.getLong("transid");

                String latest = resp.getString("latest_transaction");
                JSONObject txn = new JSONObject(latest);

                latestTransaction = new Transaction();
                latestTransaction.setMessage(txn.getString("message"));
                latestTransaction.setCode(txn.getInt("code"));
                latestTransaction.setTransId(txn.getLong("transid"));
            } catch (JSONException ex) { throw new RuntimeException(ex); }
        }
    }
}
