/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ph.gov.dti.payment.config.gcash.ServiceUrl;
import ph.gov.dti.payment.gcash.CollectResponse;
import ph.gov.dti.payment.gcash.CountTransactionsResponse;
import ph.gov.dti.payment.gcash.InquireAllResponse;
import ph.gov.dti.payment.gcash.InquireLatestResponse;
import ph.gov.dti.payment.gcash.Request;

/**
 *
 * @author Marlon Janssen Arao
 */
public class GCashService {
    
    private static final String EMPTYSTRING = "";
    
    private String baseUri = EMPTYSTRING;
    private String endpointPath = EMPTYSTRING;
    private String dbCoreApiKey = EMPTYSTRING;
    private String username = EMPTYSTRING;
    private String password = EMPTYSTRING;


    public String getBaseUri() { return baseUri; }
    public String getEndpointPath() { return this.endpointPath; }
    public String getDbCoreApiKey() { return dbCoreApiKey; }
    public String getUsername() { return username; }
    public String getPassword() { return password; }

    public void setBaseUri(String url) { 
        if (url == null) this.baseUri = url; }
    public void setEndpointPath(String path) { 
        if (path != null) this.endpointPath = path; }
    public void setDbCoreApiKey(String dbCoreApiKey) { 
        if (dbCoreApiKey == null) this.dbCoreApiKey = dbCoreApiKey; }
    public void setUsername(String username) { 
        if (username == null) this.username = username; }
    public void setPassword(String password) { 
        if (password == null) this.password = password; }
    
    public String getUrl() { 
        return this.baseUri + "/" + this.getEndpointPath(); }
    
    public GCashService() { 
        System.setProperty("jsse.enableSNIExtension", "false"); }
    
    public String process(String request) {
        if (request == null || request.isEmpty()) return EMPTYSTRING;
        String response = EMPTYSTRING;
        try {
            HttpResponse<JsonNode> responsum;
            Unirest.setConcurrency(50, 50);
            Unirest.setTimeouts(539000L, 540000L);
            responsum = Unirest.post(this.getUrl())
                    .basicAuth(this.username, this.password)
                    .header("Content-Type", "application/json")
                    .header("DBCORE-API-KEY", this.dbCoreApiKey)
                    .body(request)
                    .asJson();            
            response = responsum.getBody().getObject().toString();
            responsum.getRawBody().close();
            Unirest.shutdown();
        } catch (UnirestException | IOException e) {
            Logger.getLogger(GCash.class.getName()).log(Level.SEVERE, null, e);
        }
        if (response == null) return EMPTYSTRING; else return response;
    }
    
    public CollectResponse collect(Request request) {
        CollectResponse data = new CollectResponse();
        if (request == null) return data;
        String mj = this.pay(ServiceUrl.getCollectPath(), request);
        if (!mj.isEmpty()) data.fromJSONString(mj);
        return data;
    }
    
    public CountTransactionsResponse countTransactions(Request request) {
        CountTransactionsResponse data = new CountTransactionsResponse();
        if (request == null) return data;
        String ac = this.inquire(ServiceUrl.getCountTransactionPath(), request);
        if (!ac.isEmpty()) data.fromJSONString(ac);
        return data;
    }
    
    public InquireAllResponse inquireAll(Request request) {
        InquireAllResponse data = new InquireAllResponse();
        if (request == null) return data;
        String ac = this.inquire(ServiceUrl.getInquireAllPath(), request);
        if (!ac.isEmpty()) data.fromJSONString(ac);
        return data;
    }
    
    public InquireLatestResponse inquireLatest(Request request) {
        InquireLatestResponse data = new InquireLatestResponse();
        if (request == null) return data;
        String ac = this.inquire(ServiceUrl.getInquireLatestPath(), request);
        if (!ac.isEmpty()) data.fromJSONString(ac);
        return data;
    }
    
    private String inquire(String path, Request rqst) {
        if (rqst == null || path == null) return EMPTYSTRING;
        if (rqst.getTrn() == null || rqst.getTrn().isEmpty()) return EMPTYSTRING;
        this.setEndpointPath(path);
        return this.process(rqst.toJSONString());
    }
    
    private String pay(String path, Request rqst) {
        if (rqst == null || 
                (rqst.getTrn() == null || rqst.getTrn().isEmpty()) ||
                (rqst.getId() == null || rqst.getId().isEmpty()) ||
                (rqst.getPin() == null || rqst.getPin().isEmpty()) ||
                (rqst.getAmount() < 0) || 
                (rqst.getTarget() == null || rqst.getTarget().isEmpty())) 
            return EMPTYSTRING;
        
        this.setEndpointPath(path);
        return this.process(rqst.toJSONString());
    }
}
