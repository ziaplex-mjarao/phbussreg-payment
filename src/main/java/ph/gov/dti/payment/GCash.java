/*
 * Copyright 2016 Marlon Janssen Arao.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ph.gov.dti.payment;

import java.util.List;
import ph.gov.dti.payment.gcash.Response;
import ph.gov.dti.payment.gcash.CountTransactionsResponse;
import ph.gov.dti.payment.gcash.InquireAllResponse;
import ph.gov.dti.payment.gcash.InquireLatestResponse;
import ph.gov.dti.payment.gcash.Request;
import ph.gov.dti.payment.gcash.Transaction;

/**
 *
 * @author Marlon Janssen Arao
 */
public final class GCash extends GCashService {
    
    private Request rqst = null;
    
    public GCash(String uri) {
        super();
        this.setBaseUri(uri);
    }
    
    public GCash(String uri, String username, String password) {
        this(uri);
        this.setUsername(username);
        this.setPassword(password);
    }
    
    public long collect() {
        
        
        return -1L;
    }
    
    public int count(String trn) {
        // for invalid 
        if (trn == null || trn.isEmpty()) return -1;
        
        rqst = new Request();
        rqst.setTrn(trn);
        
        CountTransactionsResponse result = this.countTransactions(rqst);
        
        // if TRN is not found, return a negative number
        if (result.getCode() == 1019) return -1;
        
        return result.getCount();
    }
    
    public List<Transaction> inquireAll(String trn) throws ProcessingException {
        if (trn == null || trn.isEmpty())
            throw new IllegalArgumentException("TRN is null or empty.");
        
        rqst = new Request();
        rqst.setTrn(trn);
        
        InquireAllResponse rspnse = this.inquireAll(rqst);
        
        if (rspnse.getCode() != 0) manageStatus(rspnse);
        
        return rspnse.getTransactions();
    }
    
    public Transaction inquireLatest(String trn) throws ProcessingException {
        if (trn == null || trn.isEmpty())
            throw new IllegalArgumentException("TRN is null or empty.");
        
        rqst = new Request();
        rqst.setTrn(trn);
        
        InquireLatestResponse rspnse = this.inquireLatest(rqst);
        
        if (rspnse.getCode() != 0) manageStatus(rspnse);
        
        return rspnse.getLatestTransaction();
    }
    
    private void manageStatus(Response response) throws ProcessingException {
        switch (response.getCode()) {
            case 5: // DTI wallet is blacklisted or expired
            case 7: // System access is denied
            case 8: // DTI wallet supplied an incorrect PIN
            case 1020: // Not authorized
            case 1021: // This key does not have access to the requested API
            case 1023: // Invalid API key
            case 1024: // Unknown API
            case 1025: // This API does not have enough permissions
                throw new SecurityException(response.getMessage());
            case 1002: // The previous GCash request is not completed
            case 1018: // The previous GCash request is not completed
            case 3001: // Billcollect is still processing
                throw new ProcessingException(response.getMessage());
            default:
                break;
        }
    }
}
